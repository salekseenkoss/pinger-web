require 'yaml'
require 'redis'
require 'influxdb'

settings = YAML.load(File.open(File.join(File.dirname(__FILE__), "secrets.yml")))

redis_config = settings['application']['redis']
influx_config = settings['application']['influxdb']

$redis = Redis.new(
  host: redis_config['db'],
  port: redis_config['port'],
  db: (redis_config['db'] || "0")
)

$influx = InfluxDB::Client.new(
  influx_config['database'],
  host: influx_config[:host]
)
