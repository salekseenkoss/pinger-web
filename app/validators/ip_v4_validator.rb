class IPv4Validator
  IP_REGEX = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/

  attr_reader :address
  attr_accessor :errors

  def valid?
    validate
    !errors?
  end

  def validate
    validate_presence
    validate_format
    validate_unique
  end

  def initialize(address)
    @address = address
    @errors = []
  end

  def errors?
    errors.size > 0
  end

  def error_messages
    errors.join(', ')
  end

  private

  def validate_presence
    return unless address.nil? || (address.is_a?(String) && address.empty?)
    errors << 'IP is empty'
  end

  def validate_format
    return if (address =~ IP_REGEX) == 0
    errors << 'IP is not valid format'
  end

  def validate_unique
    return if IPStore.unique?(address)
    errors << 'IP already exists'
  end
end
