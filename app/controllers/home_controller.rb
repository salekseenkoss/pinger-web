require 'sinatra/json'

class HomeController < Sinatra::Base
  get '/' do
    json maps: ['/', '/ip']
  end
end
