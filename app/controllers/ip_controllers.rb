class IPController < Sinatra::Base
  
  get '/stat' do
    metrics = RequestMetricsQuery.new(
      params[:address],
      params[:from],
      params[:to]
    )

    result = metrics.query
    
    if result
      json result
    else
      metrics.error_messages
    end
  end

  post '/add' do
    address = params[:address]
    validator = IPv4Validator.new(address)

    if validator.valid?
      IPStore.add(address)
      
      json message: 'IP address added to queue'
    else
      status 422
      json errors: validator.error_messages
    end
  end

  
  delete '/remove/:address' do
    if IPStore.delete(params[:address])
      json message: 'IP address is deleted'
    else
      status 404
      json errors: 'IP address not found'
    end
  end

  configure :production, :development do
    enable :logging
  end
end
