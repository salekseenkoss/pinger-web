class IPStore
  QUEUE_NAME = 'network_address'

  class << self
    # Adds uniq item to rotation queue
    # returns Index number if item is unique
    # returns nil if not unique
    #
    def add(address = nil)
      return unless address

      $redis.rpush(QUEUE_NAME, address)
    end

    # Return last item from queue
    # Return:
    #  item # => returned value (e.g. "127.0.0.1")
    #  nil  # => if queue is empty
    #
    def pop
      $redis.lpop(QUEUE_NAME)
    end

    # Removes item from rotation queue
    # Return value:
    #  true  # => item deleted
    #  false # => nothing to delete
    # 
    def delete(address)
      $redis.lrem(QUEUE_NAME, 0, address) == 1
    end

    def unique?(address)
      !list.include?(address)
    end

    private

    # TODO: check using redis external indexes
    #
    def list
      $redis.lrange(QUEUE_NAME, 0, -1)
    end
  end
end
