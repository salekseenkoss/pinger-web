require 'date'

class RequestMetricsQuery
  MEASUREMENT_NAME = 'requests'

  attr_reader :address, :start_time, :end_time

  attr_accessor :errors

  def initialize(address, start_time, end_time)
    @errors = []
    @address = address
    
    begin
      @start_time = prepare_time start_time
      @end_time = prepare_time end_time
    rescue ArgumentError
      errors << 'Invalid date_time parameters'
    end
  end

  def valid?
    errors.size == 0
  end

  def query
    return unless valid?

    result = query_stats

    if result.any?
      result
    else
      status 404
      errors << "No any requests found for host #{address}"

      nil
    end
  end

  def error_messages
    errors.join(', ')
  end

  private

  def prepare_time(time)
    DateTime.parse(time).to_s
  end

  def query_stats
    req = "select count(rtt), mean(rtt), min(rtt), max(rtt), median(rtt), stddev(rtt) "
    req += "from #{MEASUREMENT_NAME} "
    req += "where host = '#{address}' and "
    req += "time >= '#{start_time}' and time <= '#{end_time}'"

    result = $influx.query(req)

    if result.any?
      values = result[0]['values'][0]

      bad_count = query_bad_count

      values['bad_req_percent'] = (bad_count.to_i / values['count'].to_i) * 100

      values
    else
      []
    end
  end

  def query_bad_count
    req = "select count(rtt) from #{MEASUREMENT_NAME} "
    req += "where host = '#{address}' and "
    req += "time >= '#{start_time}' and time <= '#{end_time}'"
    req += "and state = 'fail'"

    result = $influx.query(req)

    result.any? ? result[0]['values'][0]['count'] : 0
  end
end
