require 'sinatra'
require 'sinatra/base'
require 'pry-byebug'
require './config/initializer.rb'

Dir.glob('./app/{services,validators,controllers,metrics}/*.rb').each { |file| require file }

map('/') { run HomeController }
map('/ip') { run IPController }
